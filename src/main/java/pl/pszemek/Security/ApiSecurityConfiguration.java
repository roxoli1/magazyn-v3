package pl.pszemek.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;


@Configuration
@Order(1)
@EnableGlobalMethodSecurity(securedEnabled = true)
public class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {



    @Autowired
    public void configureAuth(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication().withUser("User").password("User").roles("USER")
                .and().withUser("admin").password("admin").roles("USER","ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
                 http
                         .antMatcher("/api/**")
                         .httpBasic()
                         .and()
                         .csrf().disable()
                         .authorizeRequests()
                         .antMatchers(HttpMethod.GET).hasRole("USER")
                         .antMatchers(HttpMethod.POST).hasRole("ADMIN")
                         .antMatchers(HttpMethod.PUT).hasRole("ADMIN")
                         .antMatchers(HttpMethod.DELETE).hasRole("ADMIN");
                         //.anyRequest().authenticated();
    }
}
