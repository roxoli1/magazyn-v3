package pl.pszemek.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "produkt")
public class Product {

    @Column(name = "id")
    @Id
    @GeneratedValue
    private Long id;

    private String fullName;
    private String amount;
    private String data;
    private int isPVC;
    private int isSilicon;
    private int isTeflon;
    private int isBraided;

    public Product() {

    }

    public Product(Long id, String fullName, String amount, String data) {
        this.id = id;
        this.fullName = fullName;
        this.amount = amount;
        this.data = data;
    }

    public void setPVC(int PVC) {
        isPVC = PVC;
    }

    public int getIsPVC() {
        return isPVC;
    }

    public int getIsSilicon() {
        return isSilicon;
    }

    public void setSilicon(int isSilicon) {
        this.isSilicon = isSilicon;
    }

    public int getIsTeflon() {
        return isTeflon;
    }

    public void setTeflon(int isTeflon) {
        this.isTeflon = isTeflon;
    }

    public int getIsBraided() {
        return isBraided;
    }

    public void setBraided(int isBraided) {
        this.isBraided = isBraided;
    }

    public int isPVC() {
        return isPVC;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }



}

