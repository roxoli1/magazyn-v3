package pl.pszemek.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pszemek.Entity.Product;
import pl.pszemek.repository.ProductRepository;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    @Autowired
    private ProductRepository repository;

    public Product addNewProduct(Product product) {
        return repository.save(product);
    }

    public List<Product> findAll() {
        return repository.findAll();
    }

    public Product findByFullName(String name) {
        return repository.findByFullName(name);
    }


}
