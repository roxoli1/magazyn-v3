package pl.pszemek.Service;

import pl.pszemek.Entity.Product;

import java.util.List;

public interface ProductService {
    Product addNewProduct(Product product);
    List<Product> findAll();
}
