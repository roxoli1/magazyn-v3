package pl.pszemek.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.pszemek.Entity.Product;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DeleteController {

    @RequestMapping(value = "/delete",  method = RequestMethod.POST)
    public String deleteById(HttpServletRequest request, RedirectAttributes redirectAttributes){

        String idString = request.getParameter("id");
        String searchSQL = request.getParameter("SQL");
        List<Product> productsList = new ArrayList<Product>();

        String SQL = "DELETE FROM produkt WHERE id= ";
        SQL += idString;

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/magazyn", "root", "");
            Statement statement = (Statement) connection.createStatement();
            statement.executeUpdate(SQL);

            ResultSet rs = statement.executeQuery(searchSQL);

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();

            while (rs.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = rs.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                    String columnContent = columnValue ;

                }

                System.out.println("");

                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setFullName(rs.getString("full_name"));
                product.setAmount(rs.getString("amount"));
                product.setData(rs.getString("data"));
                product.setPVC(rs.getInt("ispvc"));
                product.setBraided(rs.getInt("is_braided"));
                product.setTeflon(rs.getInt("is_teflon"));
                product.setSilicon(rs.getInt("is_silicon"));

                productsList.add(product);
            }

            redirectAttributes.addAttribute("list", productsList);
            redirectAttributes.addAttribute("SQL", searchSQL);

        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        return "redirect:searchResult";
    }
}
