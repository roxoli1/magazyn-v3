package pl.pszemek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.pszemek.Entity.Product;
import pl.pszemek.Service.ProductService;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;


@Controller
public class MagazynController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/main")
    public String getMain(){
        return "main";
    }

    @RequestMapping("/login")
    public String getLogin(){
        return "login";
    }

    @RequestMapping("/")
    public String getLogin2(){
        return "login";
    }



    @RequestMapping(value = "postAdd", method = RequestMethod.POST)
    public String getPostAdd(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String name = request.getParameter("name");
        String amount = request.getParameter("amount");
        String date = request.getParameter("date");
        String isPVC = request.getParameter("PVC");
        String isSilicon = request.getParameter("Silicon");
        String isTeflon = request.getParameter("Teflon");
        String isBraided = request.getParameter("Ekran");

        System.out.println(isPVC);

        redirectAttributes.addAttribute("name", name);
        redirectAttributes.addAttribute("amount", amount);
        redirectAttributes.addAttribute("date", date);

        Product product = new Product();
        product.setFullName(name);
        product.setAmount(amount);
        product.setData(date);


        if (isPVC == null) {
            product.setPVC(0);          // checkbox
        } else {
            product.setPVC(1);
        }

        if (isSilicon == null) {
            product.setSilicon(0);          // checkbox
        } else {
            product.setSilicon(1);
        }

        if (isTeflon == null) {
            product.setTeflon(0);          // checkbox
        } else {
            product.setTeflon(1);
        }

        if (isBraided == null) {
            product.setBraided(0);          // checkbox
        } else {
            product.setBraided(1);
        }


        productService.addNewProduct(product);
        return "redirect:result";
    }

    @RequestMapping(value = "postSearch", method = RequestMethod.POST)
    public String getPostSearch(HttpServletRequest request, RedirectAttributes redirectAttributes, Model model){
        String name = request.getParameter("name");
        name = "%"+name+"%";
        String isPVC = request.getParameter("PVC");
        String isSilicon = request.getParameter("Silicon");
        String isTeflon = request.getParameter("Teflon");
        String isBraided = request.getParameter("Ekran");
        String SQL = "SELECT * FROM produkt WHERE ";
        List<Product> productsList = new ArrayList<Product>();

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/magazyn", "root", "");

            Statement statement = (Statement) connection.createStatement();

            if(name.equals("%%") && isPVC == null && isBraided == null && isSilicon == null && isTeflon == null){
                SQL = "SELECT * FROM produkt ORDER BY full_name";

            } else if(!(name.equals("%%"))){
                SQL += "full_name LIKE '" + name + "'";

                if(isPVC != null) SQL += "AND ispvc ='" + 1 + "' ";
                if(isSilicon != null) SQL += "AND is_silicon ='" + 1 + "' ";
                if(isTeflon != null) SQL += "AND is_teflon ='" + 1 + "' ";
                if(isBraided != null) SQL += "AND is_braided ='" + 1 + "' ";
            } else if (isPVC != null ) {
                SQL +=  "ispvc ='" + 1 + "' ";

                if(isSilicon != null) SQL += "AND is_silicon ='" + 1 + "' ";
                if(isTeflon != null) SQL += "AND is_teflon ='" + 1 + "' ";
                if(isBraided != null) SQL += "AND is_braided ='" + 1 + "' ";
            } else if (isSilicon != null){
                SQL += "is_silicon ='" + 1 + "' ";

                if(isTeflon != null) SQL += "AND is_teflon ='" + 1 + "' ";
                if(isBraided != null) SQL += "AND is_braided ='" + 1 + "' ";
            } else if (isTeflon != null){
                SQL += "is_teflon ='" + 1 + "' ";
                if(isBraided != null) SQL += "AND is_braided ='" + 1 + "' ";
            } else SQL += "is_braided ='" + 1 + "' ";


/*            if(name.equals("%%") && PVC != null ) {
                SQL = "SELECT * FROM produkt WHERE ispvc ='" + 1 + "'";
            } else
                if (PVC == null) {
                    SQL = "SELECT * FROM produkt WHERE full_name LIKE '" + name + "'";
                } else {
                    SQL = "SELECT * FROM produkt WHERE ispvc = 1 AND full_name LIKE '" + name + "'";
                }
            }*/

            ResultSet rs = statement.executeQuery(SQL);

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();

            while (rs.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = rs.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                    String columnContent = columnValue ;

                }

                System.out.println("");

                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setFullName(rs.getString("full_name"));
                product.setAmount(rs.getString("amount"));
                product.setData(rs.getString("data"));
                product.setPVC(rs.getInt("ispvc"));
                product.setBraided(rs.getInt("is_braided"));
                product.setTeflon(rs.getInt("is_teflon"));
                product.setSilicon(rs.getInt("is_silicon"));

                productsList.add(product);
            }

            redirectAttributes.addAttribute("list", productsList);
            redirectAttributes.addAttribute("SQL", SQL);

            /*
            model.addAttribute("showAwesomeList", true);
            model.addAttribute("list", resultList);*/

        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return "redirect:searchResult";
    }

    @RequestMapping(value = "/searchResult")
    public String getSearchResult(@RequestParam String SQL, @RequestParam ArrayList<Product> list, Model model, RedirectAttributes redirectAttributes) {
        model.addAttribute("list", list);
        model.addAttribute("showAwesomeList", true);
        model.addAttribute("SQL", SQL);
        return "main";
    }

    @RequestMapping("/result")
    public String getResult(@RequestParam String name, @RequestParam String amount, @RequestParam String date, Model model) {
        model.addAttribute("addedName", name);
        model.addAttribute("addedAmount", amount);
        model.addAttribute("addedDate", date);
        model.addAttribute("showAdded", true);
        return "main";
    }
}
